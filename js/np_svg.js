
var np_network_icon = m("svg[viewBox='-0 -0 275 275'][xmlns='http://www.w3.org/2000/svg'][xmlns:xlink='http://www.w3.org/1999/xlink']", { style: "height: 1.5em;" }, [
    m("g[transform='translate(0,0)']", [
        m("g", [
            m("ellipse[cx='32'][cy='102'][rx='30'][ry='30'][fill='transparent'][stroke='#000000'][stroke-width='6'][transform='translate(2,3)'][opacity='0.25']"),
            m("ellipse[cx='32'][cy='102'][rx='30'][ry='30'][fill='transparent'][stroke='#000000'][stroke-width='6']"),
            m("ellipse[cx='52'][cy='202'][rx='30'][ry='30'][fill='transparent'][stroke='#000000'][stroke-width='6'][transform='translate(2,3)'][opacity='0.25']"),
            m("ellipse[cx='52'][cy='202'][rx='30'][ry='30'][fill='transparent'][stroke='#000000'][stroke-width='6']"),
            m("ellipse[cx='146'][cy='32'][rx='30'][ry='30'][fill='transparent'][stroke='#000000'][stroke-width='6'][transform='translate(2,3)'][opacity='0.25']"),
            m("ellipse[cx='146'][cy='32'][rx='30'][ry='30'][fill='transparent'][stroke='#000000'][stroke-width='6']"),
            m("ellipse[cx='146'][cy='130'][rx='30'][ry='30'][fill='transparent'][stroke='#000000'][stroke-width='6'][transform='translate(2,3)'][opacity='0.25']"),
            m("ellipse[cx='146'][cy='130'][rx='30'][ry='30'][fill='transparent'][stroke='#000000'][stroke-width='6']"),
            m("ellipse[cx='152'][cy='222'][rx='30'][ry='30'][fill='transparent'][stroke='#000000'][stroke-width='6'][transform='translate(2,3)'][opacity='0.25']"),
            m("ellipse[cx='152'][cy='222'][rx='30'][ry='30'][fill='transparent'][stroke='#000000'][stroke-width='6']"),
            m("ellipse[cx='242'][cy='132'][rx='30'][ry='30'][fill='transparent'][stroke='#000000'][stroke-width='6'][transform='translate(2,3)'][opacity='0.25']"),
            m("ellipse[cx='242'][cy='132'][rx='30'][ry='30'][fill='transparent'][stroke='#000000'][stroke-width='6']")
        ]),
        m("g", [
            m("path[d='M 131 200 L  53 123'][fill='none'][stroke='rgba(0, 0, 0, 1)'][stroke-width='6'][stroke-miterlimit='10']"),
            m("path[d='M  59  89 L 118  47'][fill='none'][stroke='rgba(0, 0, 0, 1)'][stroke-width='6'][stroke-miterlimit='10']"),
            m("path[d='M 221 110 L 167  53'][fill='none'][stroke='rgba(0, 0, 0, 1)'][stroke-width='6'][stroke-miterlimit='10']"),
            m("path[d='M 173 200 L 220 153'][fill='none'][stroke='rgba(0, 0, 0, 1)'][stroke-width='6'][stroke-miterlimit='10']"),
            m("path[d='M  83 211 L 122 222'][fill='none'][stroke='rgba(0, 0, 0, 1)'][stroke-width='6'][stroke-miterlimit='10']"),
            m("path[d='M 177 129 L 212 132'][fill='none'][stroke='rgba(0, 0, 0, 1)'][stroke-width='6'][stroke-miterlimit='10']"),
            m("path[d='M 146 152 L 152 192'][fill='none'][stroke='rgba(0, 0, 0, 1)'][stroke-width='6'][stroke-miterlimit='10']"),
            m("path[d='M  62 111 L 116 122'][fill='none'][stroke='rgba(0, 0, 0, 1)'][stroke-width='6'][stroke-miterlimit='10']"),
            m("path[d='M 131  60 L  69 175'][fill='none'][stroke='rgba(0, 0, 0, 1)'][stroke-width='6'][stroke-miterlimit='10']"),
        ])
    ])
])

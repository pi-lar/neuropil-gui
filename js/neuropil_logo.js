'use strict';

export class NeuropilLogo {

    constructor(location, width, height, with_text) {
        this.location = location;
        this.width = width;
        this.height = height;
        var dataset = [
            10, 20, 30, 40, 50, 60, 70, 80
        ]

        var colorScale = d3.scaleSequential().domain([0, 80]).interpolator(d3.interpolateRainbow);

        // let pick_color = d3.scaleSequential().domain([0, 50]).interpolator(d3.interpolateRainbow);
        // let pick_color = function (x) {
        //     return "rgb(0,0,0)"
        // };

        // create the svg area
        this.svg = d3.select("#neuropil_logo")
            .append("svg")
            .attr("width", 500)
            .attr("height", 250);

        this.svg.selectAll("path")
            .data(dataset)
            .enter().append("path")
            .attr("transform", function (d) { return "translate(100,100) rotate(" + d + ")"; })
            .attr("d", d3.arc()
                .innerRadius(function (d) { return d + d / 10; })
                .outerRadius(function (d) { return d + d / 6; })
                .startAngle(1.57)     // It's in radian, so Pi = 3.14 = bottom.
                .endAngle(6.28)       // 2*Pi = 6.28 = top
            )
            .attr('stroke', function (d) { return colorScale(d); })
            .attr('fill', function (d) { return colorScale(d); });

        if (with_text) {
            this.svg
                .append('text')
                .attr("transform", function (d) { return "translate(225, 73) rotate(180)"; })
                .attr('stroke', 'black')
                .style("font-size", "5.3em")
                .style("font-family", "Times")
                .text("N");
            this.svg
                .append('text')
                .attr('x', 225)
                .attr('y', 108)
                .attr('stroke', 'black')
                .style("font-size", "5em")
                .style("font-family", "Times")
                .text("europil");
            this.svg
                .append('text')
                .attr('x', 230)
                .attr('y', 128)
                .attr('stroke', 'black')
                .style("font-size", "1em")
                .style("font-family", "Times")
                .text("cyber-security-mesh");
        }
        d3.interval(function (elapsed) {
            d3.select("#neuropil_logo")
                .select("svg")
                .selectAll("path")
                .transition()
                .duration(250)
                .ease(d3.easeLinear)
                .attr("transform", function (d, i) {
                    var seconds = elapsed / 1000;
                    var rotate = d + (d / Math.PI * seconds) % 360;
                    if (i % 2)
                        return "translate(100,100) rotate(" + rotate + ")";
                    else
                        return "translate(100,100) rotate(" + -1 * rotate + ")";
                })
        }, 250);
    }
}
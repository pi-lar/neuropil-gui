// np_models.js

var np_nodes = {
    error_text: "",
    nodes: [],
    current_node: {},
    connect_string: "",

    loadNodes: function () {
        m.request({
            method: "GET",
            url: "http://localhost:3114/sysinfo",
            withCredentials: false,
        })
            .then(function (result) {
                np_nodes.nodes = result;
                np_nodes.current_node = result[0];
            })
            .catch(function (e) {
                if (typeof (e) == "object") np_nodes.error_text = "Request to backend failed. Have you started your node?";
                else if (e instanceof Error) np_nodes.error_text = e.message;
                else np_nodes.error_text = "fatal error, not revoverable";
            });
    },
    do_connect: function () {
        m.request({
            method: "GET",
            url: "http://localhost:3114/sysinfo",
            params: { connect_string: np_nodes.connect_string },
        })
            .then(function (result) {
                np_nodes.connect_string = "";
                np_nodes.nodes = result;
                np_nodes.current_node = result[0];
            })
            .catch(function (e) {
                if (typeof (e) == "object") np_nodes.error_text = "Request to backend failed. Have you started your node?";
                else if (e instanceof Error) np_nodes.error_text = e.message;
                else np_nodes.error_text = "fatal error, not revoverable";
            });
    }
}

var np_settings = {
    settings: {},
    error_text: "",

    loadSettings: function () {
        m.request({
            method: "GET",
            url: "http://localhost:3114/settings/",
            params: {},
            withCredentials: false,
            // deserialize: function(value) {return value}
        })
            .then(function (result) {
                // console.log(result);
                np_settings.settings = result;
            })
            .catch(function (e) {
                if (typeof (e) == "object") np_settings.error_text = "Request to backend failed. Have you started your node?";
                else if (e instanceof Error) np_settings.error_text = e.message;
                else np_settings.error_text = "fatal error, not revoverable";
            })
    },
}

var np_identities = {
    error_text: "",
    identities: [],
    loadIdentities: function () {
        // TODO: make XHR call
    }
}

var np_file = {
    error_text: "",

    np_id: "",
    name: "",
    last_modified: "",
    subject: "",

    mimetype: "",
    encoding: "",
    content: [],

    current: {},
    loadFiles: function (np_id) {
        m.request({
            method: "GET",
            url: "http://localhost:3114/files/:id",
            params: { id: np_id },
            withCredentials: false,
        })
            .then(function (result) {
                // console.log(result);
                np_file.current = result;

                np_file.np_id = result.np_id;
                np_file.name = result.name;
                np_file.path = result.path;
                np_file.last_modified = result.last_modified;
                np_file.subject = result.subject;
                np_file.encoding = result.encoding;
                np_file.mimetype = result.mimetype;
                np_file.content = result.content ? result.content : [];
            })
            .catch(function (e) {
                // ignore for now
            });
    },
}

var np_dir = {
    error_text: "",

    np_id: "",
    name: "",
    files: [],
    directories: [],
    query_text: "",

    current: {},
    // load a subdirectory
    load: function (np_id) {
        m.request({
            method: "GET",
            url: "http://localhost:3114/files/:id",
            params: { id: np_id },
            withCredentials: false,
        })
            .then(function (result) {
                console.log(result);
                np_dir.current = result;

                np_dir.np_id = result.np_id;
                np_dir.name = result.name;
                np_dir.path = result.path;
                np_dir.subject = result.subject;
                np_dir.last_modified = result.last_modified;
                np_dir.directories = result.directories ? result.directories : [];
                np_dir.files = result.files ? result.files : [];
            })
    },
    // load the initial directory setup
    loadFiles: function () {
        m.request({
            method: "GET",
            url: "http://localhost:3114/files",
            withCredentials: false,
        })
            .then(function (result) {
                console.log(result);
                np_dir.np_id = result.np_id;
                np_dir.name = result.name;
                np_dir.path = result.path;
                np_dir.subject = result.subject;
                np_dir.directories = result.directories ? result.directories : [];
                np_dir.files = result.files ? result.files : [];
            })
            .catch(function (e) {
                // ignore for now
            });
    },
    do_share: function () {
        m.request({
            method: "GET",
            url: "http://localhost:3114/files",
            params: { share_file: np_dir.query_text },
        })
            .then(function (result) {
                console.log(result);
                np_dir.query_text = ""

                np_dir.np_id = result.np_id;
                np_dir.name = result.name;
                np_dir.path = result.path;
                np_dir.subject = result.subject;
                np_dir.directories = result.directories ? result.directories : [];
                np_dir.files = result.files ? result.files : [];
            })
    }
}

var np_search = {
    error_text: "",
    query_text: "",
    search_result: [],
    loadSearch: function () {
    },
    do_query: function () {
        // console.log("do_query", np_search.query_text);
        m.request({
            method: "GET",
            url: "http://localhost:3114/search",
            params: { query_text: np_search.query_text },
            withCredentials: false,
        })
            .then(function (result) {
                np_search.search_result = result;
                // console.log(result);
            })
            .catch(function (e) {
                // ignore for now
            });
    },
}

var np_chat = {
    error_text: "",
    query_text: "",
    chat_list: [],
    loadChat: function () {
    },
    do_chat: function () {
        // console.log("do_query", np_search.query_text);
        m.request({
            method: "GET",
            url: "http://localhost:3114/chat",
            params: { query_text: np_chat.query_text },
            withCredentials: false,
        })
            .then(function (result) {
                np_chat.chat_list = result;
                // console.log(result);
            })
            .catch(function (e) {
                // ignore for now
            });
    },
}

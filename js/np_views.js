
const timeConverter = function (seconds) {
    var a = new Date(seconds * 1000);
    var months = ['Jan', 'Feb', 'Mar', 'Apr', 'May', 'Jun', 'Jul', 'Aug', 'Sep', 'Oct', 'Nov', 'Dec'];
    var year = a.getFullYear();
    var month = months[a.getMonth()];
    var date = a.getDate();
    var hour = ('0' + a.getHours()).slice(-2);
    var min = ('0' + a.getMinutes()).slice(-2);
    var sec = ('0' + a.getSeconds()).slice(-2);
    var time = date + ' ' + month + ' ' + year + ' ' + hour + ':' + min + ':' + sec;
    return time;
}

const convert_protocol = function (np_protocol) {
    if (np_protocol == 17) return 'udp6';
    if (np_protocol == 18) return 'udp4';
    if (np_protocol == 33) return 'tcp6';
    if (np_protocol == 34) return 'tcp4';
    if (np_protocol == 257) return 'pas6';
    if (np_protocol == 258) return 'pas4';
    return "UNKNOWN";
}

const construct_np_address = (node = {}) => {
    return node["np.n.k"] + ":" + convert_protocol(node["np.n.pr"]) + ":" + node["np.n.d"] + ":" + node["np.n.p"];
}

const construct_arc_attributes = (y, x = [], i) => {

    var radius = 3 * (i + 1);
    // see also
    // https://stackoverflow.com/questions/5736398/how-to-calculate-the-svg-path-for-an-arc-of-a-circle

    var start = parseInt(x[0], 16) * 360 / 256;
    var end = parseInt(x[3], 16) * 360 / 256;

    var need_large_sweep = start - end;
    var largeArcFlag = 0;
    if (need_large_sweep > 0) {
        largeArcFlag = need_large_sweep <= 180.0 ? "0" : "1";
    }
    else {
        largeArcFlag = need_large_sweep >= -180.0 ? "1" : "0";
    }
    var startAngleInRadians = (start - 90) * Math.PI / 180.0;
    var start_x = y + radius * Math.cos(startAngleInRadians);
    var start_y = y + radius * Math.sin(startAngleInRadians);

    var endAngleInRadians = (end - 90) * Math.PI / 180.0;
    var end_x = y + radius * Math.cos(endAngleInRadians);
    var end_y = y + radius * Math.sin(endAngleInRadians);

    var arc = ['M', start_x, start_y, 'A', radius, radius, 0, largeArcFlag, 0, end_x, end_y].join(' ');
    return arc;
}

const construct_arc_color = (x = []) => {
    return "rgb(" + parseInt(x[1], 16) + "," + parseInt(x[2], 16) + "," + parseInt(x[3], 16) + ")";
}

const construct_small_np_logo = (node = {}) => {

    let dataset = node["np.n.k"].match(/(.{1,8})/g);
    var size = 50;
    var center = size / 2;

    return m("svg", { width: size, height: size },
        dataset.map(function (x, i) {
            let sub_dataset = dataset[i].match(/(.{1,2})/g);
            return m("path", {
                transform: "translate(0,0) rotate(0)",  //  + parseInt(sub_dataset[0], 16) + ")",
                d: construct_arc_attributes(center, sub_dataset, i),
                stroke: construct_arc_color(sub_dataset),
                fill: "none" // construct_arc_color(sub_dataset)
            })
        })
    );
}

const construct_node_table_details = (node = {}) => {

    return m("div", { class: "tile" },
        m("div", { class: "tile-icon" },
            m("div", { class: "small-np-icon centered" },
                construct_small_np_logo(node)
            ),
        ),
        m("div", { class: "tile-content" },
            m("div", { class: "tile-title text-small" }, node["np.n.k"]),
            m("div", { class: "tile-subtitle text-gray" },
                m("small", { class: "" }, "Protocol: " + convert_protocol(node["np.n.pr"])),
                m("small", { class: "" }, "· Hostname: " + node["np.n.d"]),
                m("small", { class: "" }, "· Port: " + node["np.n.p"]),
            )
        )
    )
}

const search_by_dhkey = (arr = [], key) => {
    let selected = undefined;
    arr.forEach(element => {
        if (element.source_hash === key) {
            selected = element;
        }
    });
    if (selected === undefined)
        selected = arr[0];
    return selected;
};


// define a component
var NodeArrayComponent = {

    view: function (vnode) {
        if (Array.isArray(vnode.children) && hasOwnProperty.call(vnode.children[0], "np.n.k")) {
            return m("table", vnode.attrs,
                m("thead", [],
                    m("tr", {},
                        m("th", { class: "text-gray" }, "connect?"),
                        m("th", { class: "text-gray" }, "Node ID / details"),
                        m("th", { class: "text-gray" }, "Latency"),
                        m("th", { class: "text-gray" }, "Stability"),
                        m("th", { class: "text-gray" }, "Last Update"),
                    )
                ),
                m("tbody", {},
                    vnode.children.map(function (node) {
                        return m("tr",
                            m("td",
                                m(m.route.Link, {
                                    class: "np_node icon icon-plus",
                                    onclick: function (e) {
                                        np_nodes.connect_string = construct_np_address(node);
                                        np_nodes.do_connect();
                                    }
                                }, "")
                            ),
                            m("td", {},
                                construct_node_table_details(node),
                            ),
                            m("td",
                                m("i", { class: "np_node" },
                                    m("", {}, node["np.n.l"].toFixed(2)),
                                )
                            ),
                            m("td",
                                m("i", { class: "np_node" },
                                    node["np.n.sa"].toFixed(2),
                                )
                            ),
                            m("td",
                                m("i", { class: "np_node" },
                                    timeConverter(node["np.n.ls"])
                                )
                            ),
                            // m("td", m("i", {class: "np_node" }, timeConverter(node["np.n.c"])) ),
                        )
                    })
                )
            )
        }
        else {
            return m(".np_empty", { class: "empty col-12" },
                m("div", { class: "empty-title h5" }, "... empty space is boring ..."),
                m("div", { class: "empty-subtitle" }, "but connecting to other nodes may help"),
                m("div", { class: "input-group input-inline" },
                    m("input", {
                        class: "form-input",
                        type: "text",
                        placeholder: "Connect ...",
                        oninput: function (e) { np_nodes.connect_string = e.target.value; },
                        value: np_search.connect_string
                    },
                    ),
                    m("button", {
                        class: "btn btn-primary input-group-btn",
                        onclick: function (e) {
                            e.preventDefault();
                            np_nodes.do_connect();
                            np_nodes.connect_string = "";
                        }
                    }, "Connect")
                )
            )
        }
    }
}

var np_nodes_view = {
    // member variables
    nodes: [],
    // member functions
    oninit: np_nodes.loadNodes,
    view: function (vnode) {
        if (vnode && vnode.key) np_nodes.current_node = search_by_dhkey(np_nodes.nodes, vnode.key);
        else np_nodes.current_node = np_nodes.nodes[0];

        if (np_nodes.current_node === undefined) {
            return m(".np_nodes columns",
                m("span", { class: "h6 col-2" }, ""),
                m("span", { class: "h6 col-8" }, m("div", { class: "toast toast-warning" }, np_nodes.error_text)),
                m("span", { class: "h6 col-2", style: "text-align: end" }, np_icons.icon("share")),
                m("span", { class: "divider col-12" }, ""),

                m(".np_empty", { class: "empty col-12" },
                    m("div", { class: "empty-title h5" }, "... empty space is boring ..."),
                    m("div", { class: "empty-subtitle" }, "but connecting to other nodes may help"),
                    m("div", { class: "input-group input-inline" },
                        m("input",
                            {
                                class: "form-input",
                                type: "text",
                                placeholder: "Connect ...",
                                oninput: function (e) { np_nodes.connect_string = e.target.value; },
                                value: np_nodes.connect_string
                            },
                        ),
                        m("button",
                            {
                                class: "btn btn-primary input-group-btn",
                                onclick: function (e) {
                                    e.preventDefault();
                                    np_nodes.do_connect();
                                }
                            }, "Connect")
                    )
                )
            )
        }
        else {
            return m(".np_nodes columns",
                m("span", { class: "h6 col-2" }, np_icons.share),
                m("span", { class: "h6 col-8" }, "your node id: " + construct_np_address(np_nodes.current_node.node)),
                m("span", { class: "h6 col-2" },
                    m("div", { class: "input-group input-inline" },
                        m("input",
                            {
                                class: "form-input",
                                type: "text",
                                placeholder: "Connect ...",
                                oninput: function (e) { np_nodes.connect_string = e.target.value; },
                                value: np_nodes.connect_string
                            },
                        ),
                        m("button",
                            {
                                class: "btn btn-primary input-group-btn",
                                onclick: function (e) {
                                    e.preventDefault();
                                    np_nodes.do_connect();
                                }
                            }, "Connect")
                    ),
                ),
                m("div", { class: "divider col-12" }, ""),
                m("div", { class: "divider col-1" }, ""),
                m("div", { class: "column col-10 col-md-10" },
                    m("div", { class: "accordion" },
                        m("input", { type: "checkbox", id: "accordion-1", name: "accordion-checkbox", hidden: true }, ""),
                        m("label", { class: "accordion-header", for: "accordion-1" },
                            m("i", { class: "icon icon-arrow-right mr-1" }, ""),
                            "LEAFSET TABLE",
                        ),
                        m("div", { class: "accordion-body" },
                            m(NodeArrayComponent, { class: "table col-12 table-striped table-hover" }, np_nodes.current_node.neighbour_nodes)
                        )
                    ),
                    m("div", { class: "accordion" },
                        m("input", { type: "checkbox", id: "accordion-2", name: "accordion-checkbox", hidden: true }, ""),
                        m("label", { class: "accordion-header", for: "accordion-2" },
                            m("i", { class: "icon icon-arrow-right mr-1" }, ""),
                            "ROUTING TABLE",
                        ),
                        m("div", { class: "accordion-body" },
                            m(NodeArrayComponent, { class: "table table-striped table-hover" }, np_nodes.current_node.routing_nodes),
                        )
                    ),
                ),
                m("div", { class: "divider col-1" }, ""),
            )
        }
    }
}

var np_identities_view = {
    oninit: np_nodes.loadIdentities,
    view: function () {
        return m(".np_identities columns",
            m("span", { class: "h6 col-2" }, ""),
            m("span", { class: "h6 col-8" }, ""),
            m("span", { class: "h6 col-2", style: "text-align: end" }, np_icons.icon("user")),
            m("div", { class: "divider col-12" }, ""),

            m(".np_empty", { class: "empty col-12" }, "")

            // m(".np_identities",
            //     np_identities.nodes.map(function(identity) {
            //         return m(".np_identities_id", identity.hash + " ");
            //     })
            // )
        )
    }
}

var np_settings_view = {
    oninit: np_settings.loadSettings,
    view: function () {
        return m(".np_settings columns",
            m("span", { class: "h6 col-2" }, ""),
            m("span", { class: "h6 col-8" }, m("div", { class: "toast toast-warning" }, np_settings.error_text)),
            m("span", { class: "h6 col-2", style: "text-align: end" }, np_icons.icon("settings")),
            m("div", { class: "divider col-12" }, ""),

            m(".np_empty", { class: "empty col-12" }, "")

            // m(".np_identities",
            //     np_identities.nodes.map(function(identity) {
            //         return m(".np_identities_id", identity.hash + " ");
            //     })
            // )
        )
    }
}

function downloadFile(name, content) {
    window.open(name);
}

var np_file_view = {
    oninit: function (vnode) { console.log(vnode); np_file.load(vnode.attrs.key); },
    // onupdate: function(vnode) {np_files.load(vnode.attrs.np_id); },
    view: function () {
        return m(".np_file columns",
            m("span", { class: "h6 col-2" }, ""),
            m("span", { class: "h6 col-8" }, "current file: " + np_file.path + "/" + np_file.name),
            m("span", { class: "h6 col-2", style: "text-align: end" }, np_icons.icon("file")),
            m("div", { class: "divider col-12" }, ""),

            m("div", { class: "col-2" }, ""),
            m("div", { class: "col-10" },
                m("span", timeConverter(np_file.last_modified)),
                m("span", { class: "chip" }, np_file.mimetype),
                m("span", "urn:files:" + np_file.np_id),
                m("span", m("i", { class: "icon icon-download" }, "Download !"))
            )
            // m("table", {class: "table table-striped table-hover" }, np_file.current.map(function(file) {
            //     return m("tr",
            //                 m("td", m("i", {class : "np_file icon icon-minus" }, "" )),
            //                 m("td", m(m.route.Link, {class: ".np_file_item",  href: "/files/" + file}, "urn:files:" + file ))
            //             )
            // }))
        )
    }
}

var np_dir_view = {
    oninit: function (vnode) {
        if (vnode.attrs.key) { np_dir.load(vnode.attrs.key); }
        else { np_dir.loadFiles(); }
    },
    view: function () {
        if (np_dir.directories == null || (np_dir.directories.length == 0 && np_dir.files.length == 0)) {
            return m(".np_dir columns",
                m("span", { class: "h6 col-2" }, ""),
                m("span", { class: "h6 col-8" }, "current directory: " + "/"),
                m("span", { class: "h6 col-2", style: "text-align: end" }, np_icons.icon("folder")),
                m("div", { class: "divider col-12" }, ""),

                m(".np_empty", { class: "empty col-12" },
                    m("div", { class: "empty-title h5" }, "... empty space is boring ..."),
                    m("div", { class: "empty-subtitle" }, "but sharing files with friends may help"),
                    m("div", { class: "input-group input-inline" },
                        m("input", {
                            class: "form-input",
                            type: "text",
                            placeholder: "Share File/Folder ...",
                            oninput: function (e) { np_dir.query_text = e.target.value; },
                            value: np_dir.query_text
                        },),
                        m("button", {
                            class: "btn btn-primary input-group-btn",
                            onclick: function (e) {
                                e.preventDefault();
                                np_dir.do_share();
                            }
                        }, "Share File/Folder ...")
                    )
                )
            )
        }
        else {
            return m(".np_dir columns",
                m("span", { class: "h6 col-2" }, np_icons.file),
                m("span", { class: "h6 col-8" }, "current directory: " + np_dir.path),
                m("span", { class: "h6 col-2" },
                    m("div", { class: "input-group input-inline" },
                        m("input", {
                            class: "form-input",
                            type: "text",
                            placeholder: "Share File/Folder ...",
                            oninput: function (e) { np_dir.query_text = e.target.value; },
                            value: np_dir.query_text
                        },),
                        m("button", {
                            class: "btn btn-primary input-group-btn",
                            onclick: function (e) {
                                e.preventDefault();
                                np_dir.do_share();
                            }
                        }, "Share File/Folder ...")
                    )
                ),
                m("div", { class: "divider col-12" }, ""),

                m("div", { class: "col-2" }, ""),
                m("div", { class: "divider col-10 text-center", "data-content": "DIRECTORIES" }, ""),
                m("div", { class: "col-2" }, ""),
                m("div", { class: "col-10" },
                    m("table", { class: "table table-striped table-hover" }, np_dir.directories.map(function (dir) {
                        return m("tr",
                            m("td", m("i", { class: "np_dir" }, np_icons.folder)),
                            m("td", m(m.route.Link, { class: "np_dir_item", href: "/files/" + dir }, "urn:files:" + dir))
                        )
                    }))),
                m("div", { class: "col-2" }, ""),
                m("div", { class: "divider col-10 text-center", "data-content": "FILES" }, ""),
                m("div", { class: "col-2" }, ""),
                m("div", { class: "col-10" },
                    m("table", { class: "table table-striped table-hover" }, np_dir.files.map(function (file) {
                        return m("tr",
                            m("td", m("i", { class: "np_file" }, np_icons.file)),
                            m("td", m(m.route.Link, { class: "np_file_item", href: "/file/" + file }, "urn:files:" + file))
                        )
                    })))
            )
        }
    }
}

var np_search_view = {
    oninit: np_search.loadSearch,
    view: function () {
        if (np_search.search_result == null || np_search.search_result.length == 0) {
            return m(".np_search columns",
                m("span", { class: "h6 col-2" }, ""),
                m("span", { class: "h6 col-8" }, np_search.error_text ? m("div", { class: "toast toast-warning" }, np_search.error_text) : ""),
                m("span", { class: "h6 col-2", style: "text-align: end" }, np_icons.icon("search")),
                m("div", { class: "divider col-12" }, ""),

                m(".np_empty", { class: "empty col-12" },
                    m("div", { class: "empty-title h5" }, "... empty space is boring ..."),
                    m("div", { class: "empty-subtitle" }, "but searching for content may help"),
                    m("div", { class: "input-group input-inline" },
                        m("input", {
                            class: "form-input",
                            type: "text",
                            placeholder: "Search ...",
                            oninput: function (e) { np_search.query_text = e.target.value; },
                            value: np_search.query_text
                        },),
                        m("button", {
                            class: "btn btn-primary input-group-btn",
                            onclick: function (e) {
                                e.preventDefault();
                                np_search.search_result = [];
                                np_search.do_query();
                            }
                        }, "Search")
                    )
                )
            )

        }
        else {
            return m(".np_search columns",
                m("span", { class: "h2 col-2" }, np_icons.search),
                m("span", { class: "h6 col-8" }, ""),
                m("span", { class: "h6 col-2" },

                    m("div", { class: "input-group input-inline" },
                        m("input", {
                            class: "form-input",
                            type: "text",
                            placeholder: "Search even more ...",
                            oninput: function (e) { np_search.query_text = e.target.value; },
                            value: np_search.query_text
                        },),
                        m("button", {
                            class: "btn btn-primary input-group-btn",
                            onclick: function (e) {
                                e.preventDefault();
                                np_search.search_result = [];
                                np_search.do_query();
                            }
                        }, "Search")
                    )
                ),
                m("div", { class: "divider col-12" }, ""),
                m("table", { class: "table table-striped table-hover" },
                    m("thead", {},
                        m("tr", {},
                            m("th", {}, ""),
                            m("th", {}, "hitcount"),
                            m("th", {}, "probability"),
                            m("th", {}, "label"),
                            m("th", {}, "title")
                        )
                    ),
                    m("tbody", {}, np_search.search_result.map(function (search_item) {
                        return m("tr",
                            m("td", m("i", { class: "np_search icon icon-search" }, "")),
                            m("td", m("i", { class: "np_search" }, search_item.hit_counter)),
                            m("td", m("i", { class: "np_search" }, search_item.similarity)),
                            m("td", m("i", { class: "np_search" }, search_item.label)),
                            m("td", m("i", { class: "np_search" }, search_item.title)),
                        )
                    })
                    )
                )
            )
        }
    }
}

var np_chat_view = {
    oninit: np_chat.loadChat,
    view: function () {
        if (np_chat.chat_list == null || np_chat.chat_list.length == 0) {
            return m(".np_chat columns",
                m("span", { class: "h6 col-2" }, ""),
                m("span", { class: "h6 col-8" }, np_chat.error_text ? m("div", { class: "toast toast-warning" }, np_chat.error_text) : ""),
                m("span", { class: "h6 col-2", style: "text-align: end" }, np_icons.icon("chat")),
                m("div", { class: "divider col-12" }, ""),

                m(".np_empty", { class: "empty col-12" },
                    m("div", { class: "empty-title h5" }, "... empty space is boring ..."),
                    m("div", { class: "empty-subtitle" }, "but chatting with peers may help"),
                    m("div", { class: "input-group input-inline" },
                        m("input", {
                            class: "form-input",
                            type: "text",
                            placeholder: "Chat ...",
                            oninput: function (e) { np_chat.query_text = e.target.value; },
                            value: np_chat.query_text
                        },),
                        m("button", {
                            class: "btn btn-primary input-group-btn",
                            onclick: function (e) {
                                e.preventDefault();
                                np_chat.chat_list = [];
                                np_chat.do_chat();
                            }
                        }, "Chat")
                    )
                )
            )
        }
        else {
            return m(".np_chat columns",
                m("span", { class: "h6 col-2" }, np_icons.chat),
                m("span", { class: "h6 col-8" }, ""),
                m("div", { class: "h6 col-2" },
                    m("div", { class: "input-group input-inline" },
                        m("input",
                            {
                                class: "form-input",
                                type: "text",
                                placeholder: "Chat with more ...",
                                oninput: function (e) { np_char.query_text = e.target.value; },
                                value: np_search.query_text
                            },
                        ),
                        m("button",
                            {
                                class: "btn btn-primary input-group-btn",
                                onclick: function (e) {
                                    e.preventDefault();
                                    np_char.chat_list = [];
                                    np_char.do_chat();
                                }
                            }, "Chat")
                    ),
                ),
                m("div", { class: "divider col-12" }, ""),
                m("table", { class: "table table-striped table-hover" },
                    m("thead", {},
                        m("tr", {},
                            m("th", {}, ""),
                            m("th", {}, "type"),
                            m("th", {}, "active"),
                            m("th", {}, "label"),
                            m("th", {}, "title")
                        )
                    ),
                    m("tbody", {}, np_chat.chat_list.map(function (chat_item) {
                        return m("tr",
                            m("td", m("i", { class: "np_chat icon icon-search" }, "")),
                            m("td", m("i", { class: "np_chat" }, chat_item.type)),
                            m("td", m("i", { class: "np_chat" }, chat_item.active)),
                            m("td", m("i", { class: "np_chat" }, chat_item.label)),
                            m("td", m("i", { class: "np_chat" }, chat_item.title)),
                        )
                    })
                    )
                )
            )
        }
    }
}

var np_empty_view = {
    view: function () {
        return m(".np_empty columns",
            m("span", { class: "h6 col-2" }, ""),
            m("span", { class: "h6 col-8" }, ""),
            m("span", { class: "h6 col-2", style: "text-align: end" }, np_icons.icon("home")),
            m("div", { class: "divider col-12" }, ""),

            m(".np_empty", { class: "empty col-12" },
                m("div", { class: "empty-title h5" }, "... empty space is boring ..."),
                m("div", { class: "empty-subtitle" }, "but clicking the buttons may help"),
                m(m.route.Link, { class: "empty-action btn", href: "/identities" }, "create/load an identity"),
                m(m.route.Link, { class: "empty-action btn", href: "/nodes" }, "join a network"),
                m(m.route.Link, { class: "empty-action btn", href: "/files" }, "share some files"),
                m(m.route.Link, { class: "empty-action btn", href: "/search" }, "search for content"),
            )
        )
    }
}

var np_layout = {
    view: function (vnode) {
        return m(".np_layout", { class: "container grid-xl" }, [
            m(".neuropil_logo", { "id": "neuropil_logo" }),
            m("svg", { "viewBox": "-25 -25 200 200", class: "np_menu", "xmlns:xlink": "http://www.w3.org/1999/xlink" }, [
                // m("g", {class: "menuitem" }, "")
            ]),
            // m(".np_menu", {class: "tab tab-block" }, [
            //     m(m.route.Link, {class: "tab-item navbar-brand", href: "/"}, "np://"),
            //     m(m.route.Link, {class: "tab-item", href: "/identities"}, m("i", {class: "icon icon-people" }, "Identities") ),
            //     m(m.route.Link, {class: "tab-item", href: "/nodes"}, np_network_icon),
            //     m(m.route.Link, {class: "tab-item", href: "/files"}, "np_files_view"),
            //     m(m.route.Link, {class: "tab-item", href: "/search"}, m("i", {class: "icon icon-search" }, "Search")),
            // ]),
            m(".np_content", vnode.children)
        ])
    }
}


var np_icons = {

    // icons: {},
    icon_path: {
        "circle": './images/svg/circle.svg',
        "home": './images/svg/home.svg',
        "folder": './images/svg/folder.svg',
        "file": './images/svg/file.svg',
        "link": './images/svg/link.svg',
        "chat": './images/svg/message-circle.svg',
        "search": './images/svg/search.svg',
        "settings": './images/svg/settings.svg',
        "share": './images/svg/share-2.svg',
        "user": './images/svg/user.svg',
    },
    readTextFile: function (file, encoding) {
        let text_svg = "";
        let rawFile = new XMLHttpRequest();
        rawFile.open("GET", file, false);
        rawFile.onreadystatechange = function () {
            if (rawFile.readyState === 4) {
                if (rawFile.status === 200 || rawFile.status == 0) {
                    text_svg = rawFile.responseText;
                }
            }
        }
        rawFile.send(null);
        // return m.trust(text_svg);
        return text_svg;
    },

    load_icons: function () {
        np_icons.icons = {}
        for (const [name, path] of Object.entries(np_icons.icon_path)) {
            np_icons.icons[name] = np_icons.readTextFile(path, "utf-8");
        }
    },

    icon: function (name) {
        // console.log(name, np_icons.icons);
        if (np_icons.icons === undefined)
            np_icons.load_icons()
        return m.trust(np_icons.icons[name]);

        var file = new File();
        file.name = filename;
        var reader = FileReader();
        var svg = reader.readAsText(file, encoding);
        return m.trust(svg);
    },
};
# neuropil-gui

A small html gui that uses the localhost interface of the core neuropil library.
Used as a tool for administrating the local neuropil installation (load identities, share files, display network).
The gui should work with HTTP GET only! 
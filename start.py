#!/usr/bin/env python3
from http.server import SimpleHTTPRequestHandler 
from socketserver import ForkingTCPServer,TCPServer

from urllib.request import urlopen

class MyProxy(SimpleHTTPRequestHandler):
    def do_GET(self):
        print(self.path)

        if "3114" in self.path:
            url=f"http://localhost:3114{self.path[5:]}"
            print(url)
            self.send_response(200)
            self.end_headers()
            self.copyfile(urlopen(url), self.wfile)
        else:
            super().do_GET()

PORT = 8081

with TCPServer(("0.0.0.0", PORT), MyProxy) as httpd:
    print("Server started at http://0.0.0.0:" + str(PORT))
    httpd.serve_forever()
